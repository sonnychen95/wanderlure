//
//  ProfileViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 8/29/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit
import MapKit



class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UINavigationControllerDelegate {
    
    var profileDrill = Int()
    let imagePicker = UIImagePickerController()
    
    var tableContent = ["Bookmarks", "My Reviews", "Submit Location", "Logout"]
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profilePicture: UIImageView!
    
    @IBOutlet weak var profileTableView: UITableView!
    
       override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        
        //View Controller is delegate for imagePicker
        imagePicker.delegate = self
        
        //Back Segue
               self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        
        //profile picture
        profilePicture.layer.cornerRadius = 55
        profilePicture.clipsToBounds = true
        
        updateUser()
        
    }
    
    //Upload Profile Picture 
    
    func profpic() {
        
        let alert = UIAlertController(title: nil, message: nil,
            preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let camera = UIAlertAction(title:"Take Photo",
            style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .Camera
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        
        let library = UIAlertAction(title:"Choose Photo",
            style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .PhotoLibrary
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title:"Cancel",
            style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        alert.addAction(camera)
        alert.addAction(library)
        alert.addAction(cancel)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func updateUser() {
        //Current User
        let currentUser = PFUser.currentUser()
        
        if currentUser != nil {
            profileView.hidden = false
            
            if let pictureFile = currentUser!["profile_picture"] as? PFFile {
                pictureFile.getDataInBackgroundWithBlock {
                    (imageData: NSData?, error: NSError?) -> Void in
                    if error == nil {
                        self.profilePicture.image = UIImage(data:imageData!)
                        
                    }}}
            currentUser?.fetchInBackground()
            let first = currentUser!["first"] as? String
            let last = currentUser!["last"] as? String
            name.text = first! + " " + last!
            
        } else {
            
            profilePicture.image = nil
            profileView.hidden = true
            
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableContent.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("profileCell")! as UITableViewCell
        
        cell.textLabel!.text = tableContent[indexPath.row]
        
        return cell
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "profileDrill") {
        let theDestination = segue.destinationViewController as! CustomTableViewController
        
        if (profileDrill == 0){
            theDestination.ParseClass = "Attraction"
            theDestination.key = "Been"
            theDestination.navigationTitle = "Places I've Explored"
        } else if (profileDrill == 1){
            theDestination.ParseClass = "Attraction"
            theDestination.key = "Saved"
            theDestination.navigationTitle = "Bookmarks"
        }  else if (profileDrill == 2){
            theDestination.ParseClass = "Reviews"
            theDestination.key = "author"
            theDestination.navigationTitle = "Reviews"
        }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row == 0) {
            profileDrill = 0
            performSegueWithIdentifier("profileDrill", sender: self)
        } else if (indexPath.row == 1) {
            profileDrill = 1
            performSegueWithIdentifier("profileDrill", sender: self)
        } else if (indexPath.row == 2) {
            profileDrill = 2
            performSegueWithIdentifier("profileDrill", sender: self)
        }
        else if (indexPath.row == 3) {
            Logout()
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    
    func Logout() {
        
        let alert = UIAlertController(title: nil, message: nil,
            preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let logoutAction = UIAlertAction(title:"Logout",
            style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
                PFUser.logOut()
                self.updateUser()
        }
        let logoutCancel = UIAlertAction(title:"Cancel",
            style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addAction(logoutAction)
        alert.addAction(logoutCancel)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }

   

    
    
    //causes orientation problem
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextProfile: UIImage = UIImage(CGImage: image.CGImage!)
        
        let contextSize: CGSize = contextProfile.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextProfile.CGImage, rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: image.scale, orientation: .Up)
      
        
        
        return image
    }
    
    //pick image
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let pickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let scaledProfile = cropToBounds(pickedImage, width: 200, height: 200)
            let imageData = UIImageJPEGRepresentation(scaledProfile, 100)
        let imageFile:PFFile = PFFile(data: imageData!)
        PFUser.currentUser()!.setObject(imageFile, forKey: "profile_picture")
        PFUser.currentUser()!.saveInBackgroundWithBlock {
            (success, error) -> Void in
            if (success) {
                self.profilePicture.image = scaledProfile
            } else {
                print("error")
            }
        }
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    //navigation on image picker
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if navigationController.isKindOfClass(UIImagePickerController.self) {
            viewController.navigationController!.navigationBar.translucent = false
            viewController.edgesForExtendedLayout = .None
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)

           
        }
        
        
    }
    
    
    
   

    

       

}




