//
//  ListTour1.swift
//  Wanderlure
//
//  Created by Sonny Chen on 12/6/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class ListTour1: PFQueryTableViewController {
    
    var Attraction: PFObject!

    // Initialise the PFQueryTable tableview
    override init(style: UITableViewStyle, className: String!) {
        super.init(style: style, className: className)
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        // Configure the PFQueryTableView
        self.pullToRefreshEnabled = true
        self.paginationEnabled = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
    }
    
    // Define the query that will provide the data for the table view
    override func queryForTable() -> PFQuery{
        let query = PFQuery(className: "UnitedStates")
        query.orderByAscending("State")
        return query
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        performSegueWithIdentifier("listTour2", sender: self)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "listTour2"){
            let indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow!
            let object: PFObject = self.objects![indexPath.row] as! PFObject
           let theDestination = segue.destinationViewController as! ListTour2
           theDestination.Attraction = object
            }
        
    }

    
    // Cell for Row
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell {
        
        
        var listTour1 = tableView.dequeueReusableCellWithIdentifier("listTour1Cell") as! CustomTableViewCell!
        if listTour1 == nil {
            listTour1 = CustomTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "listTour1Cell")
        }
        
        if let attractionName = object?["name"] as? String {
            listTour1.customText.text = attractionName
            
            if let attractionImage = object?["image"] as? PFFile {
                attractionImage.getDataInBackgroundWithBlock {
                    (imageData: NSData?, error: NSError?) -> Void in
                    if error == nil {
                        listTour1.customImage.image = UIImage(data:imageData!)
                        
                        
                    }
                }
            }
        }
    return listTour1
    }
}
