//
//  ViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 7/30/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
   
    var usersLocation: PFGeoPoint?
    var annotation:MKAnnotation!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    var nameArray = [String]()
    var imageArray = [UIImage]()
    let blue = UIColor(red:88.0/255.0, green: 190.0/255.0, blue: 204.0/255.0, alpha: 1.0)
    let gray = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
    var selectedName = String()
    var selectedImage = UIImage()
    var area = PFGeoPoint()
    var SearchSender = Int()
    var locationManager: CLLocationManager?
    var searchResults = ["California", "Arizona", "Greece", "Chile", "China"]
    var searchResultsPhotos = [UIImage(named: "California.png"), UIImage(named: "Arizona.png"), UIImage(named: "Greece.png"), UIImage(named: "Chile.png"), UIImage(named: "China.png")]
      

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var list: UITableView!
    @IBOutlet weak var Searchbar: UISearchBar!
    @IBOutlet weak var searchDisplay: UITableView!
    @IBOutlet weak var segmentedControl: UIBarButtonItem!
    @IBOutlet weak var currentLocationButton: RoundedButton!
    @IBOutlet weak var refreshButton: RoundedButton!
    @IBOutlet weak var callOutView: UIView!
    @IBOutlet weak var callOutImage: UIImageView!
    @IBOutlet weak var callOutLabel: UILabel!
    
    
    @IBAction func Refresh(sender: AnyObject) {
        SearchSender = 2
        self.searchBarSearchButtonClicked(Searchbar)
    }
    
    @IBAction func tapCallOut(sender: AnyObject) {
        selectedName = callOutLabel.text!
        selectedImage = callOutImage.image!
        performSegueWithIdentifier("viewDetail", sender: self)
    }
    
    @IBAction func currentLocation(sender: AnyObject) {
        if (map.userLocation.location != nil) {
        let userLocation = map.userLocation
        let region = MKCoordinateRegionMakeWithDistance(
            userLocation.location!.coordinate, 7000, 7000)
        
        map.setRegion(region, animated: true)
        }
    }
    
//Segmented Control
    @IBAction func viewToggle(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            list.hidden = true
            map.hidden = false
            currentLocationButton.enabled = true
            refreshButton.enabled = true
            callOutView.frame.origin.x = 0
           
        case 1:
           list.hidden = false
           map.hidden = true
           currentLocationButton.enabled = false
           refreshButton.enabled = false
            callOutView.frame.origin.x = 700
            
        default:
            break;
        }
    }
    
//Hide navigation bar returning from detail view
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBarHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//Map Default
        self.map.showsPointsOfInterest = false
        self.map.showsBuildings = false
        map.tintColor = blue
        let span = MKCoordinateSpanMake(0.8, 0.8)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 37.773972, longitude:  -122.431297), span: span)
        self.map.setRegion(region, animated: false)
        
//Callout Hidden
        callOutView.hidden = true
        
//Connect 2nd Table View to DataSource and Delegate
        self.searchDisplay.dataSource = self
        self.searchDisplay.delegate = self

    
// Remove back button text
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        
//Search Display Background
        self.searchDisplay.backgroundView = nil
        let transparentWhite = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.95)
        self.searchDisplay.backgroundColor = transparentWhite
        

            
//Hide Search Display
            searchDisplay.hidden = true
            
            
//Change Search Bar placeholder UNEDITTED
        let text = self.Searchbar.valueForKey("searchField") as? UITextField
        text!.font =  UIFont (name: "BrandonGrotesque-Regular", size: 17)
        text!.textColor = blue
        for subView in Searchbar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString("Explore by location", comment:""), attributes:[NSForegroundColorAttributeName: gray])
                    }
                }
            }
        
        
        
//Current User Location
        PFGeoPoint.geoPointForCurrentLocationInBackground { point, error in
            if error == nil {
                self.usersLocation = point
            }
        }
        
//Blend Search Bar Border
       
        self.Searchbar.layer.borderWidth = 1
        self.Searchbar.layer.borderColor = blue.CGColor
        self.Searchbar.backgroundImage = UIImage(named: "blue.png")
    }
    
//Custom Annotation Images
    func mapView(mapView: MKMapView, didAddAnnotationViews views: [MKAnnotationView]) {
        
        var i = -1;
        for view in views {
            i++;
            let mkView = view 
            if view.annotation is MKUserLocation {
                continue;
            }
            
            // Check if current annotation is inside visible map rect, else go to next one
            let point:MKMapPoint = MKMapPointForCoordinate(mkView.annotation!.coordinate);
            
            let endFrame:CGRect = mkView.frame;
            
            // Move annotation out of view
            mkView.frame = CGRectMake(mkView.frame.origin.x, mkView.frame.origin.y - self.view.frame.size.height, mkView.frame.size.width, mkView.frame.size.height);
            
            // Animate drop
            let delay = 0.01 * Double(i)
            UIView.animateWithDuration(0.5, delay: delay, options: UIViewAnimationOptions.CurveEaseIn, animations:{() in
                mkView.frame = endFrame
                // Animate squash
                }, completion:{(Bool) in
                    UIView.animateWithDuration(0.03, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations:{() in
                        mkView.transform = CGAffineTransformMakeScale(1.0, 0.85)
                        
                        }, completion: {(Bool) in
                            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations:{() in
                                mkView.transform = CGAffineTransformIdentity
                                }, completion: nil)
                    })
                    
            })
        }
    }
    
//Map Annotations
    func mapView(mapView: MKMapView,viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let reusePin = "pin"
        var pinView = map.dequeueReusableAnnotationViewWithIdentifier(reusePin) as? MKPinAnnotationView
        if (annotation.isKindOfClass(MKUserLocation))
        {
            return nil
        }
        pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reusePin)
        let pinAppearance = UIImage(named: "MapPin")
        pinView!.image = pinAppearance
        return pinView
        }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        callOutView.hidden = false
        annotation = self.map.selectedAnnotations[0] 
        callOutLabel.text = annotation.title!
        
        
                   }
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        callOutView.hidden = true
    }

//Search Results Count
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.list) {
         return imageArray.count
        } else {
        return searchResults.count
        }
    }
    
//Table --> Detail
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (tableView == self.list) {
        selectedName = self.nameArray[indexPath.row]
        selectedImage = self.imageArray[indexPath.row]
        performSegueWithIdentifier("viewDetail", sender: self)
        } else {
            SearchSender = 1
           self.searchBarSearchButtonClicked(Searchbar)
        }
    }
    

//Pass Data to Detail View
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {


        if(segue.identifier == "viewDetail"){
        
            let theDestination = segue.destinationViewController as! DetailViewController
           
    }
    }
    
//Dequeue Cell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("attractionCell") as! AttractionCell


        
        if (tableView == self.list) {
        self.list.rowHeight = 90
        cell.attractionName.text = nameArray[indexPath.row]
        cell.attractionImage.image = imageArray[indexPath.row]
        } else {
        cell.backgroundColor = UIColor.clearColor()
        cell.currentLocation.text = searchResults[indexPath.row]
        cell.searchResultsPhotos.image = searchResultsPhotos[indexPath.row]
        cell.searchResultsPhotos.layer.cornerRadius = 5
        }
        return cell
       
    }
    
//SearchBar Font + Placeholder EDITTED
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
        searchDisplay.hidden = false
        Searchbar.setShowsCancelButton(true, animated: true)
        
        let text = self.Searchbar.valueForKey("searchField") as? UITextField
        text!.font =  UIFont (name: "BrandonGrotesque-Bold", size: 17)
        for subView in Searchbar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString("Explore where?", comment:""), attributes:[NSForegroundColorAttributeName: gray])
                }
            }
        }
    }

// SearchBar Cancel Button
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        resignFirstResponder()
        searchDisplay.hidden = true
        self.view.endEditing(true)
        Searchbar.setShowsCancelButton(false, animated: true)
        
        //Revert placeholder to original
        let text = self.Searchbar.valueForKey("searchField") as? UITextField
        text!.font =  UIFont (name: "BrandonGrotesque-Regular", size: 17)
        text!.textColor = blue
        for subView in Searchbar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString("Explore by location", comment:""), attributes:[NSForegroundColorAttributeName: gray])
                }
            }
        }
    }


//Searching Attractions
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        //Dismiss Search Bar and remove annotations
        Searchbar.resignFirstResponder()
        dismissViewControllerAnimated(true, completion: nil)
        
        
        nameArray.removeAll()
        imageArray.removeAll()
        list.reloadData()
        searchDisplay.hidden = true

     
        if self.map.annotations.count != 0{
        self.map.removeAnnotations(map.annotations)
        }
        
        
        //2
        
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = Searchbar.text
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.startWithCompletionHandler { (localSearchResponse, error) -> Void in
            
        //3
            
        //Search Bar
    if (self.SearchSender == 1) {
            self.area = PFGeoPoint(latitude:self.usersLocation!.latitude, longitude: self.usersLocation!.longitude)
            self.map.centerCoordinate = CLLocationCoordinate2D(latitude: self.usersLocation!.latitude, longitude: self.usersLocation!.longitude)
        } else if (self.SearchSender == 2) {
            self.area = PFGeoPoint(latitude: self.map.centerCoordinate.latitude, longitude: self.map.centerCoordinate.longitude)
        }
    else {
            self.pointAnnotation = MKPointAnnotation()
            self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
            self.area = PFGeoPoint(latitude:localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
            self.map.centerCoordinate = self.pointAnnotation.coordinate
            }
            

        self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
            
            let annotationQuery = PFQuery(className:"Attraction")
            annotationQuery.whereKey("Location", nearGeoPoint:self.area, withinMiles: 50)
            annotationQuery.limit = 20
            annotationQuery.findObjectsInBackgroundWithBlock {
                    (objects: [AnyObject]?, error: NSError?) -> Void in
                    
                    if error == nil {
                        // Do something with the found objects
                        let objects = objects as! [PFObject]
                        for object in objects {
                            let point = object["Location"] as! PFGeoPoint
                            let title = object["name"] as! String
                           
                            if let image = object["image"] as? PFFile {
                                image.getDataInBackgroundWithBlock {
                                    (imageData: NSData?, error: NSError?) -> Void in
                                    if error == nil {
                                        self.imageArray.append(UIImage(data:imageData!)!)
                                        self.nameArray.append(title)
                                        
                                        let annotation = MKPointAnnotation()
                                        annotation.coordinate = CLLocationCoordinate2DMake(point.latitude, point.longitude)
                                        annotation.title = title

                                        self.map.addAnnotation(annotation)
                            }
                                    
                            self.list.reloadData()
                            self.SearchSender = 0
                            self.Searchbar.setShowsCancelButton(false, animated: true)
                            }
        }
    }
    }
                    }
}
}
}
