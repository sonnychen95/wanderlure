//
//  CustomTableViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 11/7/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class CustomTableViewController: PFQueryTableViewController {
    
    var ParseClass = String()
    var key = String()
    var navigationTitle = String()
    
    var userID = PFUser.currentUser()?.objectId
    
        override func viewDidLoad() {
        super.viewDidLoad()
            
            self.navigationItem.title = navigationTitle
            
       
    }
    
    override func queryForTable() -> PFQuery{
        //Query
        let query = PFQuery(className:ParseClass)
        query.whereKey(key, equalTo: userID!)
    return query
    }

    
   override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell {
        let customCell = tableView.dequeueReusableCellWithIdentifier("customCell") as! CustomTableViewCell
        
        if let attractionName = object?["name"] as? String {
            customCell.customText.text = attractionName
            
            // Display Image
            if let attractionImage = object?["image"] as? PFFile {
                attractionImage.getDataInBackgroundWithBlock {
                    (imageData: NSData?, error: NSError?) -> Void in
                    if error == nil {
                        customCell.customImage.image = UIImage(data:imageData!)
                        
                    }
                }
            }
    }
    return customCell
    }
    
    }
