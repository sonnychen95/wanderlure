//
//  AttractionCell.swift
//  Wanderlure
//
//  Created by Sonny Chen on 8/30/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit

class AttractionCell: UITableViewCell {
    
    @IBOutlet weak var attractionImage: UIImageView!
   
    @IBOutlet weak var currentLocation: UILabel!
    @IBOutlet weak var attractionName: UILabel!
    @IBOutlet weak var searchResultsPhotos: UIImageView!
    

  

}