//
//  BorderedView.swift
//  Wanderlure
//
//  Created by Sonny Chen on 10/22/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class BorderedView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let gray = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
        self.layer.borderColor = gray.CGColor
        self.layer.borderWidth = 1

        
    }

}
