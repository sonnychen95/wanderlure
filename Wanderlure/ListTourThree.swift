//
//  ListTourThree.swift
//  Wanderlure
//
//  Created by Sonny Chen on 1/15/16.
//  Copyright © 2016 Sonny Chen. All rights reserved.
//

import UIKit

class ListTourThree: UIViewController, UITextViewDelegate {

    @IBOutlet weak var detail: UITextView!
    @IBOutlet weak var price: UITextField!
    @IBOutlet weak var maxguests: UITextField!
    
    @IBOutlet weak var placeholder: UILabel!
    var tourTitle = String()
    var tourInclusions = String()
    var tourPrice = Int()
    var tourMaxGuests = Int()
    var gettingThere = String()
    var beforeYouGo = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detail.delegate = self
    
        
        if (navigationItem.title == "Title") {
            placeholder.text = "  Name of your tour"
            detail.becomeFirstResponder()
            price.hidden = true
            maxguests.hidden = true
            
            
        } else if (navigationItem.title == "Tour Inclusions") {
            placeholder.text = "  What is included? What are the times? etc"
            detail.becomeFirstResponder()
            price.hidden = true
            maxguests.hidden = true

            
        } else if (navigationItem.title == "Price") {
            detail.hidden = true
            placeholder.hidden = true
            price.hidden = false
            maxguests.hidden = true
            price.keyboardType = UIKeyboardType.NumberPad
            price.becomeFirstResponder()
            
        } else if (navigationItem.title == "Max Guests") {
            detail.hidden = true
            placeholder.text = "  How much is the tour per group?"
            price.hidden = true
            maxguests.keyboardType = UIKeyboardType.NumberPad
            maxguests.becomeFirstResponder()
        } else if (navigationItem.title == "Getting There") {
            
            placeholder.text = "  How are your guests getting there? Hotel Pickup?"
            price.hidden = true
            maxguests.hidden = true
            
        } else if (navigationItem.title == "Before you go") {
            
            placeholder.text = "  What should your guests bring or know beforehand?"
            price.hidden = true
            maxguests.hidden = true
        }
    }
    
    func textViewDidChange(textField: UITextView) {
        if detail.text == "" {
            placeholder.hidden = false
        } else {
            placeholder.hidden = true
        }
    }
}
