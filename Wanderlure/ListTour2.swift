//
//  ListTour2.swift
//  Wanderlure
//
//  Created by Sonny Chen on 12/6/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class ListTour2: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var tourRequirements = ["Title", "Tour Inclusions", "Price", "Max Guests", "Getting There", "Before you go"]
    var Attraction: PFObject?
    var Tour: PFObject?
    let imagePicker = UIImagePickerController()
    var passTitle = String()
    
    var tourTitle = String()
    var tourInclusions = String()
    var tourPrice = Int()
    var tourMaxGuests = Int()
    var gettingThere = String()
    var beforeYouGo = String()
    
    @IBOutlet weak var tourImage: UIImageView!
    @IBOutlet weak var requirementsTableView: UITableView!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var photoContainer: UIView!
    
    @IBAction func addPhoto(sender: AnyObject) {
        addPhoto()
    }
    @IBAction func clickDone(sender: AnyObject) {
        Done()
    }

   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Remove Empty Cells
        self.requirementsTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        if (Tour != nil) {
            let photo = Tour!["image"] as? PFFile
            photo!.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    self.addPhotoButton.setTitle(nil, forState: .Normal)
                   self.tourImage.image = UIImage(data:imageData!)
   
                }}

        }
            self.navigationItem.title = "Details"

        //View Controller is delegate for imagePicker
        imagePicker.delegate = self
       
        
        //Clip Image
        tourImage.clipsToBounds = true
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tourRequirements.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("tourCell")! as UITableViewCell
        
      cell.textLabel!.text = tourRequirements[indexPath.row]
        
        return cell
    }

    
    func addPhoto() {
        
        let alert = UIAlertController(title: nil, message: nil,
            preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let library = UIAlertAction(title:"Choose Photo",
            style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .PhotoLibrary
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title:"Cancel",
            style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
            2
        }
        
        alert.addAction(library)
        alert.addAction(cancel)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let pickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        tourImage.image = pickedImage
        addPhotoButton.setTitle(nil, forState: .Normal)
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as UITableViewCell!
    
        passTitle = tourRequirements[indexPath.row]
            
        performSegueWithIdentifier("requirementDetails", sender: self)
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "requirementDetails"){
            let theDestination = segue.destinationViewController as! ListTourThree
            theDestination.navigationItem.title = passTitle
        }
    }

    
    func Done() {
        let tour = PFObject(className:"Tour")
        let imageData = UIImageJPEGRepresentation(tourImage.image!, 100)
        let imageFile:PFFile = PFFile(data: imageData!)
        tour["image"] = imageFile
        
        
        tour.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                self.navigationController?.popToRootViewControllerAnimated(true)
            } else {
                print("error")
            }
    }

}
}