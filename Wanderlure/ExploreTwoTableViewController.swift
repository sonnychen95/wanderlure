//
//  ExploreTwoTableViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 12/30/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class ExploreTwoTableViewController: UITableViewController {
    
    var stateAttractions = [PFObject]()
    var state = String()
    
    @IBAction func mapSegue(sender: AnyObject) {
        performSegueWithIdentifier("mapSegue", sender: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = state
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateAttractions.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let attractionCell = tableView.dequeueReusableCellWithIdentifier("attractionCell") as! ExploreCell!
        
        let attractionObject = stateAttractions[indexPath.row]
        
        let attractionName = attractionObject["Name"] as! String
        
        if let photo = attractionObject["Image"] as? PFFile {
            photo.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    attractionCell.exploreImage.image = UIImage(data:imageData!)
                }}}
        
        attractionCell.exploreName.text = attractionName
        
        return attractionCell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "tourSegue"){
            
            let indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow!
            let object: PFObject = self.stateAttractions[indexPath.row]
            
            let theDestination = segue.destinationViewController as! BookingViewController
            theDestination.attraction = object
            
        } else if (segue.identifier == "mapSegue") {
            
            let MapSegueNav = segue.destinationViewController as! UINavigationController
            let MapSegue = MapSegueNav.topViewController as! MapViewController
            MapSegue.stateAttractions = stateAttractions
            MapSegue.state = state
            
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ExploreCell
        
        performSegueWithIdentifier("tourSegue", sender: self)
   
    }

    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offsetY = self.tableView.contentOffset.y
        for cell in self.tableView.visibleCells as! [ExploreCell] {
            let x = cell.exploreImage.frame.origin.x
            let w = cell.exploreImage.bounds.width
            let h = cell.exploreImage.bounds.height
            let y = ((offsetY - cell.frame.origin.y) / h) * 10
            cell.exploreImage.frame = CGRectMake(x, y, w, h)
        }
    }

}
