//
//  TourDetailViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 12/12/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class TourDetailViewController: UIViewController {
    
    var tour: PFObject!
    var guide: PFObject!
    var summary: String!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tourImage: UIImageView!
    
    @IBOutlet weak var guidePhotoContainer: UIView!
    @IBOutlet weak var guidePhoto: UIImageView!
    
    @IBOutlet weak var tourTitle: UILabel!
    
    @IBOutlet weak var tourPrice: UILabel!
    @IBOutlet weak var guideName: UILabel!
    
    @IBOutlet weak var tourSummary: UITextView!
    @IBOutlet weak var bookButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Tour Detail"
        
        //Scroll View
        scrollView.contentSize.height = 900
        
        //Guide Photo
        guidePhotoContainer.layer.cornerRadius = 50
        guidePhotoContainer.layer.borderColor = UIColor.whiteColor().CGColor
        guidePhotoContainer.layer.borderWidth = 1
        guidePhotoContainer.clipsToBounds = true
        
        
        if let image = tour["image"] as? PFFile {
            image.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    self.tourImage.image = UIImage(data:imageData!)
                }}}
        
        if let guideImage = guide["profile_picture"] as? PFFile {
            guideImage.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    self.guidePhoto.image = UIImage(data:imageData!)
                }}}
        
        tourTitle.text = tour["title"] as? String
        
        summary = tour["summary"] as? String
        
        tourSummary.text = summary
        
        let price = tour["price"]!
        tourPrice.text = "$" + String(price)
        
        let name = guide["first"] as! String
        guideName.text = "Led by " + name
        
        
       
    }
}
