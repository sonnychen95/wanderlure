//
//  ReviewCell.swift
//  Wanderlure
//
//  Created by Sonny Chen on 11/11/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellText: UITextView!
    @IBOutlet weak var cellAuthor: UILabel!
    @IBOutlet weak var cellDate: UILabel!
    @IBOutlet weak var imageContainer: UIView!
    
    override func awakeFromNib() {
        self.cellImage?.clipsToBounds = true
        self.cellImage.layer.cornerRadius = 30
    }
}
