//
//  ExploreTableViewController2.swift
//  Wanderlure
//
//  Created by Sonny Chen on 8/28/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit

class ExploreTableViewController2: PFQueryTableViewController {
    
    var CountryAttractions = [PFObject]()
    var getCountry = String()
    var passName = String()
    var passImage = UIImage()
    var blue = UIColor(red: 15.0/255.0, green: 175.0/255.0, blue: 195.0/255.0, alpha: 1.0)
    var getLocation = PFGeoPoint()
   
    
    @IBAction func exploreMap(sender: AnyObject) {
        
        performSegueWithIdentifier("exploreMap", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
       
        if (getLocation == PFGeoPoint(latitude: 0, longitude: 0)) {
            self.navigationItem.title = getCountry
        } else {
            self.navigationItem.title = "Nearby"
           
        }
               self.loadObjects()
    }

   

     // Initialise the PFQueryTable tableview
    override init(style: UITableViewStyle, className: String!) {
        super.init(style: style, className: className)
           }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        // Configure the PFQueryTableView
        self.pullToRefreshEnabled = true
        self.paginationEnabled = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)

    }
    
    
       // Define the query that will provide the data for the table view
    override func queryForTable() -> PFQuery{
        let query = PFQuery(className: "Attraction")
        
        if (getLocation == PFGeoPoint(latitude: 0, longitude: 0)) {
            query.whereKey("Country", equalTo: getCountry)
            query.orderByAscending("State")
        } else {
            query.whereKey("Location", nearGeoPoint: self.getLocation, withinMiles: 300)
        }
        query.limit = 20
        return query
}
    
    
    
    
    //override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ExploreCell
        
        passName = cell.exploreName.text!
        passImage = cell.exploreImage.image!
      
        performSegueWithIdentifier("exploreShow", sender: self)
        
       


    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "exploreShow"){
            
            let indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow!
            let object: PFObject = self.objects![indexPath.row] as! PFObject
            
            let theDestination = segue.destinationViewController as! DetailViewController
            theDestination.attraction = object
        
        } else if (segue.identifier == "exploreMap") {
            
            let MapSegue = segue.destinationViewController as! MapViewController
            MapSegue.Country = getCountry
            MapSegue.getObjects = CountryAttractions
            
        }

    }

    
     override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell {
        
        if (indexPath.row == 0) {
        for object in objects! {
            CountryAttractions.append(object as! PFObject)
        }
        }

        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as! ExploreCell!
        if cell == nil {
            cell = ExploreCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        
        //Attraction Number
        cell.exploreNumber.text = String(indexPath.row+1)
        
        // Display Name
    
        if let exploreName = object?["name"] as? String {
           
            cell.exploreName.text = exploreName
           
           
        
        
        if let exploreState = object?["State"] as? String {
            cell.distance.text = exploreState

        
        
        // Display Image
            if let exploreImage = object?["image"] as? PFFile {
                exploreImage.getDataInBackgroundWithBlock {
                    (imageData: NSData?, error: NSError?) -> Void in
                    if error == nil {
                        cell.exploreImage.image = UIImage(data:imageData!)
                        
                        
                        }
                }
            }}}
        
        return cell
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        //parallax
        let offsetY = self.tableView.contentOffset.y
        for cell in self.tableView.visibleCells as! [ExploreCell] {
            let x = cell.exploreImage.frame.origin.x
            let w = cell.exploreImage.bounds.width
            let h = cell.exploreImage.bounds.height
            let y = ((offsetY - cell.frame.origin.y) / h) * 10
            cell.exploreImage.frame = CGRectMake(x, y, w, h)
        }
    }
    
    

}
