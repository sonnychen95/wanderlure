//
//  SignUpViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 8/5/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController {

    //Dismiss Modal
    @IBAction func dismissSignUp(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
   
   
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
   
   
    @IBAction func signUpAction(sender: AnyObject) {
  SignUp()
        
    }
    
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(candidate)
    }
    
    func SignUp(){
        let user = PFUser()
        user["first"] = firstName.text!
        user["last"] = lastName.text!
        user.username = emailTF.text!
        user.email = emailTF.text!
        user.password = passwordTF.text!
        
        if (validateEmail(emailTF.text!) == false || passwordTF.text!.utf16.count < 7) {
            let alert = UIAlertView(title: "Try again", message: "Make sure your email is valid and password is at least 7 characters", delegate: self,cancelButtonTitle: "ok")
            alert.show()
            return
        } else {
        user.signUpInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            if error == nil {
                 self.dismissViewControllerAnimated(true, completion: nil)
            
            } else {
                let alert = UIAlertView(title: nil, message: "The email is already associated with an account", delegate: self,cancelButtonTitle: "ok")
                alert.show()
            }
        }
        }}
}
