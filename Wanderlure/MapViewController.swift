//
//  MapViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 10/20/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    let locationManager = CLLocationManager()
    var stateAttractions = [PFObject]()
    var state = String()
    var point = PFGeoPoint()

    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func currentLocation(sender: AnyObject) {
        
        if (mapView.userLocation.location != nil) {
            let userLocation = mapView.userLocation
            let region = MKCoordinateRegionMakeWithDistance(
                userLocation.location!.coordinate, 10, 10)
            
            mapView.setRegion(region, animated: true)
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Navigation Title
        if (state != "") {
            self.navigationItem.title = state
        } else {
            self.navigationItem.title = "Nearby"
        }
        
        
        //Add Annotation
        for object in stateAttractions {
            
            point = object["Location"] as! PFGeoPoint
            let title = object["Name"] as! String
            let imageFile = object["Image"] as! PFFile
            
            let annotation = CustomPointAnnotation()
            
            
            annotation.image = imageFile
            annotation.coordinate = CLLocationCoordinate2DMake(point.latitude, point.longitude)
            annotation.title = title
            
            self.mapView.addAnnotation(annotation)
        }
        
        
        //Map Default
        self.mapView.showsPointsOfInterest = false
        self.mapView.showsBuildings = false
        
        let span = MKCoordinateSpanMake(15, 15)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: point.latitude, longitude:  point.longitude), span: span)
        self.mapView.setRegion(region, animated: false)
        
            }
    
    //view annotation
    func mapView(mapView: MKMapView,viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reusePin = "pin"
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reusePin)
        if pinView == nil {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reusePin)
            pinView!.canShowCallout = true
        }
        else {
            pinView!.annotation = annotation
        }
        
        if (annotation.isKindOfClass(MKUserLocation))
        {
            return nil
        }
        
        let cpa = annotation as! CustomPointAnnotation
        
        cpa.image.getDataInBackgroundWithBlock {
            (imageData: NSData?, error: NSError?) -> Void in
            if error == nil {
                let contextImage = UIImage(data:imageData!)!
                
                
                let scaledImage = self.scaleUIImageToSize(contextImage, size: CGSizeMake(50,50))
                
                pinView!.image = scaledImage
                pinView!.layer.borderWidth = 2
                pinView!.layer.borderColor = UIColor.whiteColor().CGColor
                pinView!.layer.shadowOffset = CGSizeZero
                pinView!.layer.shadowColor = UIColor.blackColor().CGColor
                pinView!.layer.shadowRadius = 2
                pinView!.layer.shadowOpacity = 0.3;
            }}
        return pinView
    }

    
    //Scale
    func scaleUIImageToSize(let image: UIImage, let size: CGSize) -> UIImage {
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.drawInRect(CGRect(origin: CGPointZero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
  
}


    
    





