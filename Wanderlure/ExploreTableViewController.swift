//
//  ExploreTableViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 12/28/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit


class ExploreTableViewController: UITableViewController {
    
    var states = [PFObject]()
    var attractions = [PFObject]()
    var stateAttractions = [PFObject]()
    var stateName = String()
    
    override func viewDidAppear(animated: Bool) {
        stateAttractions = []
    }
    
    func refresh(sender:AnyObject) {
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Pull to Refresh
        self.refreshControl?.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        
        //Parse Query
        let query = PFQuery(className:"UnitedStates")
        query.orderByAscending("State")
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                let objects = objects as! [PFObject]
                for object in objects {
                    if (object["IsAState"] as? String == "1") {
                        self.states.append(object)
                    } else if (object["IsAState"] == nil) {
                        self.attractions.append(object)
                    }
                    self.tableView.reloadData()
    }}}
    
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return states.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       let stateCell = tableView.dequeueReusableCellWithIdentifier("stateCell") as! ExploreCell!
        
        let stateObject = states[indexPath.row]
        let stateName = stateObject["Name"] as! String
        var stateListings = Int()
        
        for object in attractions {
            if object["State"] as! String == stateName {
                stateListings++
            }
        }
        
        if let photo = stateObject["Image"] as? PFFile {
            photo.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    stateCell.countryImage.image = UIImage(data:imageData!)
                }}}

        stateCell.stateName.text = stateName

        if stateName == "Become a Guide" {
            stateCell.numberOfListings.text = "Lead a Tour"
            stateCell.numberOfListings.backgroundColor = UIColor(red: 88.0/255.0, green: 190.0/255.0, blue: 204.0/255.0, alpha: 0.7)
        } else if stateName == "Found Beauty?" {
            stateCell.numberOfListings.backgroundColor = UIColor(red: 88.0/255.0, green: 190.0/255.0, blue: 204.0/255.0, alpha: 0.7)
            stateCell.numberOfListings.text = "Submit Location"
        } else {
        stateCell.numberOfListings.text = String(stateListings) + " Tour Sites"
        stateCell.numberOfListings.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.15)
        }
        
        return stateCell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "stateSegue"){
            let theDestination = segue.destinationViewController as! ExploreTwoTableViewController
            theDestination.state = stateName
            theDestination.stateAttractions = stateAttractions
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ExploreCell
        self.stateName = cell.stateName.text!
        
        if cell.stateName.text == "Become a Guide" {
            tabBarController?.selectedIndex = 2
        } else {
 
        for object in attractions {
            if (object["State"] as? String == stateName) {
               self.stateAttractions.append(object)
            }
        }
            self.tableView.reloadData()
            performSegueWithIdentifier("stateSegue", sender: self)

        }
        
        
        
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offsetY = self.tableView.contentOffset.y
        for cell in self.tableView.visibleCells as! [ExploreCell] {
            let x = cell.countryImage.frame.origin.x
            let w = cell.countryImage.bounds.width
            let h = cell.countryImage.bounds.height
            let y = ((offsetY - cell.frame.origin.y) / h) * 10
            cell.countryImage.frame = CGRectMake(x, y, w, h)
        }
    }
   
}