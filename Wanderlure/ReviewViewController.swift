//
//  ReviewViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 11/10/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    
    var attraction: PFObject!
    let lightgray = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    
    @IBOutlet weak var reviewText: UITextView!
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    
    @IBAction func submitReview(sender: AnyObject) {
        let review = PFObject(className:"Reviews")
      review["author"] = PFUser.currentUser()
        review["review"] = reviewText.text
        review["attraction"] = attraction
        
       
        review.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                self.dismissViewControllerAnimated(true, completion: nil)
            } else {
               print("error")
            }
        }

    }
    
    
    @IBOutlet weak var navbar: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reviewText.layer.cornerRadius = 5
        reviewText.layer.borderWidth = 2
        
        reviewText.layer.borderColor = lightgray.CGColor
    }
}
