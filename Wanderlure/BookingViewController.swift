//
//  BookingViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 11/14/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class BookingViewController: UIViewController {
    
    var attraction: PFObject!
    var toursArray = [PFObject]()
    var guide: PFObject!
    var maxGuests = Int()
    var maxPrice = Int()
    
    @IBOutlet weak var bookingTableView: UITableView!
    
    @IBAction func filter(sender: AnyObject) {
        performSegueWithIdentifier("filter", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Remove Empty Cells
        self.bookingTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        let tourQuery = PFQuery(className:"Tour")
        tourQuery.whereKey("attraction", equalTo: attraction)
        tourQuery.includeKey("guide")
        
         tourQuery.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                self.toursArray = objects as! [PFObject]
            }
           self.bookingTableView.reloadData()
        }

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toursArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("bookingCell") as! BookingCell
        
        let tourObject = toursArray[indexPath.row]
        guide = tourObject["guide"] as! PFUser
        
        if let guideProfile = guide["profile_picture"] as? PFFile {
            guideProfile.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    cell.hostPhoto.image = UIImage(data:imageData!)
                }}}

        if let tourImage = tourObject["image"] as? PFFile {
            tourImage.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    cell.bookingImage.image = UIImage(data:imageData!)
                    cell.bookingCost.text = "$ " + String(tourObject["price"] as! Int)
                }}}
        
        cell.bookingDescription.text = tourObject["title"] as? String
           return cell
            }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "tourDetail"){
            
            let indexPath: NSIndexPath = bookingTableView.indexPathForSelectedRow!
            let object: PFObject = toursArray[indexPath.row]
            
            let theDestination = segue.destinationViewController as! TourDetailViewController
            theDestination.tour = object
            theDestination.guide = object["guide"] as! PFUser
        }
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("tourDetail", sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    

}
