//
//  OverlayLabel.swift
//  Wanderlure
//
//  Created by Sonny Chen on 9/10/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit

class OverlayLabel: UILabel {
    
    override func drawTextInRect(rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 23.0, right: 0.0)
        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
        
    }

}
