//
//  ListTourOne.swift
//  Wanderlure
//
//  Created by Sonny Chen on 1/13/16.
//  Copyright © 2016 Sonny Chen. All rights reserved.
//

import UIKit

let headerTitles = ["Arizona", "California", "Nevada", "New Mexico", "Oregon", "Texas", "Utah", "Virginia", "Washington", "West Virginia", "Wyoming"]
var TourSites = [Arizona, California, Nevada, NewMexico, Oregon, Texas, Utah, Virginia, Washington, WestVirginia, Wyoming]

var attractions = [PFObject]()
var Arizona = [String]()
var California = [String]()
var Nevada = [String]()
var NewMexico = [String]()
var Oregon = [String]()
var Texas = [String]()
var Utah = [String]()
var Virginia = [String]()
var Washington = [String]()
var WestVirginia = [String]()
var Wyoming = [String]()

var selectedAttraction = String()


class ListTourOne: UIViewController {
    
    @IBOutlet weak var tourSitesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let query = PFQuery(className:"UnitedStates")
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                attractions = objects as! [PFObject]
                for object in objects! {
                    if (object["IsAState"] as? String != "1") {
                        
                        if (object["State"] as? String == "Arizona") {
                            TourSites[0].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "California") {
                             TourSites[1].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "Nevada") {
                            TourSites[2].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "New Mexico") {
                             TourSites[3].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "Oregon") {
                             TourSites[4].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "Texas") {
                             TourSites[5].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "Utah") {
                            TourSites[6].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "Virginia") {
                             TourSites[7].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "Washington") {
                            TourSites[8].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "West Virginia") {
                             TourSites[9].append((object["Name"] as? String)!)
                        } else if (object["State"] as? String == "Wyoming") {
                            TourSites[10].append((object["Name"] as? String)!)                        }
                    }
                   self.tourSitesTableView.reloadData()
                }
            }
            
}

    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return TourSites.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TourSites[section].count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < headerTitles.count {
            return headerTitles[section]
        }
        return nil
    }
    
   func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("listTourOneCell") as UITableViewCell!

       cell.textLabel!.text = TourSites[indexPath.section][indexPath.row]
    
    return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "listTour2"){
            let theDestination = segue.destinationViewController as! ListTour2
            
            
            for attraction in attractions {
                if (attraction["Name"] as! String == selectedAttraction) {
                    theDestination.Attraction = attraction
                }}}}
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedAttraction = TourSites[indexPath.section][indexPath.row]
        performSegueWithIdentifier("listTour2", sender: self)
    }

}
