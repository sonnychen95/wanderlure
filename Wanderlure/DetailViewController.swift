//
//  DetailViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 9/4/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit
import QuartzCore
import MapKit

class DetailViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    var alert = UIAlertView()
    var span = MKCoordinateSpan()
    var region = MKCoordinateRegion()
    var attraction: PFObject!
    var ReviewsArray = [PFObject]()
    var ReviewText = [String]()
    var currentUser = PFUser.currentUser()
    var userID = PFUser.currentUser()?.objectId
    let blue = UIColor(red:88.0/255.0, green: 190.0/255.0, blue: 204.0/255.0, alpha: 1.0)
    var point = PFGeoPoint()
    
    @IBOutlet weak var review: UIButton!
    
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var beenButton: UIButton!
    @IBOutlet weak var attractionTitle: UILabel!
    @IBOutlet weak var attractionDescription: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var stateCountry: UILabel!
    @IBOutlet var detailImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapView: MKMapView!
   
    @IBAction func imageSlider(sender: AnyObject) {
        performSegueWithIdentifier("imageSlider", sender: self)
    }
    
    @IBAction func sliderZoom(sender: UISlider) {
        let sliderValue = CLLocationDegrees(sender.value)
        span = MKCoordinateSpanMake(sliderValue, sliderValue)
        region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: self.point.latitude, longitude: self.point.longitude), span: span)
        self.mapView.setRegion(region, animated: true)
        
    }
    
    
    @IBAction func saveButton(sender: AnyObject) {
        if (currentUser == nil) {
            let alert = UIAlertView()
            alert.message = "You must log in to your profile first"
            alert.addButtonWithTitle("OK")
            alert.show()
        } else {
            if (attraction["Bookmark"]?.containsObject(userID!) == true) {
                attraction.removeObject(userID!, forKey: "Bookmark")
                saveButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
            } else {
                attraction.addUniqueObject(userID!, forKey: "Bookmark")
                saveButton.setTitleColor(blue, forState: .Normal)
            }
        }
        attraction.saveInBackground()
    }
    @IBAction func beenButton(sender: AnyObject) {
            if (currentUser == nil) {
                let alert = UIAlertView()
                alert.message = "You must log in to your profile first"
                alert.addButtonWithTitle("OK")
                alert.show()
            } else {
                if (attraction["Been"]?.containsObject(userID!) == true) {
                     attraction.removeObject(userID!, forKey: "Been")
                    beenButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                } else {
                    attraction.addUniqueObject(userID!, forKey: "Been")
                    beenButton.setTitleColor(blue, forState: .Normal)
                }
                }
        attraction.saveInBackground()
    }
    
    @IBAction func getDirections(sender: AnyObject) {
        
        let regionDistance:CLLocationDistance = 15000
        let coordinates = CLLocationCoordinate2DMake(point.latitude, point.longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = attraction["Name"] as? String
        mapItem.openInMapsWithLaunchOptions(options)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Satellite 
        mapView.mapType = MKMapType.Satellite
        
        //Description
        attractionTitle.text = attraction["Name"] as? String
        
        let description = attraction["Description"] as! String
        let reviewCount = String(ReviewsArray.count)
        
        attractionDescription.text = description + " • " + reviewCount + " Reviews"
        
        //Query
        let reviewQuery = PFQuery(className:"Reviews")
    
            reviewQuery.whereKey("attraction", equalTo: attraction)
        reviewQuery.includeKey("author")
            reviewQuery.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                if error == nil {
                    self.ReviewsArray = objects as! [PFObject]
                }
             self.reviewTableView.reloadData()
            }

        let state = attraction["State"] as! String
    
        
        
        
        scrollView.contentSize.height = 1400
        self.navigationItem.title = attraction["Name"] as? String
        
        mapView.tintColor = blue
        mapView.userInteractionEnabled = false
      
        if let imageFile = attraction["Image"] as? PFFile {
            imageFile.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    self.detailImage.image = UIImage(data:imageData!)
                }}}
        //Attraction Map
        self.point = attraction["Location"] as! PFGeoPoint
        span = MKCoordinateSpanMake(0.1, 0.1)
        self.mapView.centerCoordinate = CLLocationCoordinate2DMake(self.point.latitude, self.point.longitude)
        region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: self.point.latitude, longitude: self.point.longitude), span: span)
        self.mapView.setRegion(region, animated: true)
       
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2DMake(self.point.latitude, self.point.longitude)
        self.mapView.addAnnotation(annotation)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
}
    
    //Require login
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        
        if identifier == "review" {
        if (currentUser == nil) {
            
            alert.message = "Please log in first"
            alert.addButtonWithTitle("OK")
            alert.show()
            return false
        } else {
            return true
        }}
        
        return true
    }
    
    
    //Pass Attraction ID to Review
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "review"){
            
            let theDestination = segue.destinationViewController as! UINavigationController
            
            let reviewVC = theDestination.viewControllers.first as! ReviewViewController
            
            reviewVC.attraction = attraction
        } else if (segue.identifier == "imageSlider") {
            
            let imageSliderVC = segue.destinationViewController as! ImageSliderViewController
            imageSliderVC.Attraction = attraction
            
            
        } else if (segue.identifier == "tours") {
            
        }
        
    }
    
   //Custom Annotation Image
    func mapView(mapView: MKMapView,viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("pin")
        
  
        annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        if (annotation.isKindOfClass(MKUserLocation)) {
            return nil
        }
        annotationView!.image = UIImage(named: "MapPin")
        return annotationView
    }
    
    
    //Dequeue Table VIew
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("reviewCell") as! ReviewCell
        
        if tableView == reviewTableView {
        
        let reviewObject = ReviewsArray[indexPath.row]
        let user = reviewObject["author"] as! PFUser
        
        if let userProfile = user["profile_picture"] as? PFFile {
            userProfile.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                   cell.cellImage.image = UIImage(data:imageData!)
                }}}
        
       cell.cellDate.text = "4 days ago"
        cell.cellAuthor.text = user["first"] as? String
        cell.cellText.text = reviewObject["review"] as! String
        
        } 
        return cell
    }
    
    //Search Results Count
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return ReviewsArray.count
    }
    
    }


//needed for contains
extension Array {
    func contains<T where T : Equatable>(obj: T) -> Bool {
        return self.filter({$0 as? T == obj}).count > 0
    }
}