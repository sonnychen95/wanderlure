//
//  LoginViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 8/5/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit
import Parse
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {

    
    @IBAction func dismissLogin(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    @IBOutlet weak var userText: UITextField!
    @IBOutlet weak var passText: UITextField!
    @IBAction func loginButton(sender: AnyObject) {
        Login()
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if (FBSDKAccessToken.currentAccessToken() == nil) {
            print("Not logged in..")
        }
        else {
            print("Logged in!")
        }
        
        let loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email"]
        
        loginButton.delegate = self
        
        self.view.addSubview(loginButton)
        
    }
        
        func Login(){
            var user = PFUser()
            user.username = userText.text!
            user.password = passText.text!
            
          PFUser.logInWithUsernameInBackground(userText.text!, password: passText.text!) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            else {
                let failLogin = UIAlertView()
                failLogin.message = "Whoops, that was wrong. Try again."
                failLogin.addButtonWithTitle("OK")
                failLogin.show()
            }
            }
            
            
}
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if error == nil {
            print("Log in complete.")
            self.performSegueWithIdentifier("showNew", sender: self)
        }
        else {
            print(error.localizedDescription)
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User logged out.")
    }
}