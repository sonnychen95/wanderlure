//
//  SettingTableViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 8/29/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import  UIKit

class SettingTableViewController: UITableViewController {

    @IBOutlet weak var logoutLabel: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    
  var currentUser = PFUser.currentUser()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userEmail.text = currentUser?.email
    
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
                    print("success", terminator: "")
        PFUser.logOut()
        var currentUser = PFUser.currentUser()
        navigationController?.popViewControllerAnimated(true)
        
    }
}
