//
//  CustomTableViewCell.swift
//  Wanderlure
//
//  Created by Sonny Chen on 11/7/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class CustomTableViewCell: PFTableViewCell {

    @IBOutlet weak var customImage: UIImageView!
    @IBOutlet weak var customText: UILabel!
    
    @IBOutlet weak var imageHolder: UIView!
    
    override func awakeFromNib() {
        self.customImage.clipsToBounds = true
    }
}
