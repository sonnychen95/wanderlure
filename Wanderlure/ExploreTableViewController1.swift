//
//  ExploreTableViewController1.swift
//  Wanderlure
//
//  Created by Sonny Chen on 10/15/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit

class ExploreTableViewController1: PFQueryTableViewController {
    
    var states = [PFObject]()
    var passCountry = String()
    let blue = UIColor(red:88.0/255.0, green: 190.0/255.0, blue: 204.0/255.0, alpha: 1.0)
    let searchView = UITableView()
    var usersLocation = PFGeoPoint()
    var nearbyOrCountry = Int()

    @IBAction func exploreNearby(sender: AnyObject) {
        nearbyOrCountry = 1
        performSegueWithIdentifier("countrySegue", sender: self)
    }
        
     override func viewDidLoad() {
        super.viewDidLoad()
  
        //Current User Location
        PFGeoPoint.geoPointForCurrentLocationInBackground { point, error in
            self.usersLocation = point!
                    }
        
        //index scrollbar color
        let white = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.6)
        self.tableView.sectionIndexColor = white
        self.tableView.sectionIndexBackgroundColor = UIColor.clearColor()
        
    }
    
    // Initialise the PFQueryTable tableview
    override init(style: UITableViewStyle, className: String!) {
        super.init(style: style, className: className)
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    // Define the query that will provide the data for the table view
    override func queryForTable() -> PFQuery{
        let query = PFQuery(className: "UnitedStates")
        
        if self.objects!.count == 0 {
            query.cachePolicy = .CacheThenNetwork
        }
        
        query.orderByAscending("State")
        return query
    }
    
    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        return ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","R","S","T","U","V","Y","Z"]
}
    
    
    override func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        
        //n-1 * 300
            //A
        if index == 0 {
            self.tableView.setContentOffset(CGPointMake(0, 0), animated: false)
            //B
        } else if index == 1 {
            self.tableView.setContentOffset(CGPointMake(0, 300), animated: false)
            //C
        } else if index == 2 {
            self.tableView.setContentOffset(CGPointMake(0, 1200), animated: false)
            //D
        } else if index == 3 {
            self.tableView.setContentOffset(CGPointMake(0, 2700), animated: false)
            //E
        } else if index == 4 {
            self.tableView.setContentOffset(CGPointMake(0, 3000), animated: false)
            //F
        } else if index == 5 {
            self.tableView.setContentOffset(CGPointMake(0, 3300), animated: false)
            //G
        } else if index == 6 {
            self.tableView.setContentOffset(CGPointMake(0, 3600), animated: false)
            //H
        } else if index == 7 {
            self.tableView.setContentOffset(CGPointMake(0, 4200), animated: false)
            //I
        } else if index == 8 {
            self.tableView.setContentOffset(CGPointMake(0, 4500), animated: false)
            //J
        } else if index == 9 {
            self.tableView.setContentOffset(CGPointMake(0, 5400), animated: false)
            //K
        } else if index == 10 {
            self.tableView.setContentOffset(CGPointMake(0, 5700), animated: false)
            //L
        } else if index == 11 {
            self.tableView.setContentOffset(CGPointMake(0, 6000), animated: false)
            //M
        } else if index == 12 {
            self.tableView.setContentOffset(CGPointMake(0, 6300), animated: false)
            //N
        } else if index == 13 {
            self.tableView.setContentOffset(CGPointMake(0, 6600), animated: false)
            //O
        } else if index == 14 {
            self.tableView.setContentOffset(CGPointMake(0, 6900), animated: false)
            //P
        } else if index == 15 {
            self.tableView.setContentOffset(CGPointMake(0, 7200), animated: false)
            //R
        } else if index == 16 {
            self.tableView.setContentOffset(CGPointMake(0, 7800), animated: false)
            //S
        } else if index == 17 {
            self.tableView.setContentOffset(CGPointMake(0, 8100), animated: false)
            //T
        } else if index == 18 {
            self.tableView.setContentOffset(CGPointMake(0, 8400), animated: false)
            //U
        } else if index == 19 {
            self.tableView.setContentOffset(CGPointMake(0, 8700), animated: false)
            //V
        } else if index == 20 {
            self.tableView.setContentOffset(CGPointMake(0, 9000), animated: false)
            //Y
        } else if index == 21 {
            self.tableView.setContentOffset(CGPointMake(0, 9300), animated: false)
            //Z
        } else if index == 22 {
            self.tableView.setContentOffset(CGPointMake(0, 9340), animated: false)
        }
        return 1
    }
    
//Did Select Row
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        nearbyOrCountry = 2
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ExploreCell
        passCountry = cell.countryName.text!
        performSegueWithIdentifier("countrySegue", sender: self)
    }
    
//Prepare for Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let theDestination = segue.destinationViewController as! ExploreTableViewController2
        if(nearbyOrCountry == 2){
            theDestination.getCountry = passCountry
        } else if (nearbyOrCountry == 1){
            theDestination.getLocation = usersLocation
        }
    }

//Cell for Row
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell {
        
        var countryCell = tableView.dequeueReusableCellWithIdentifier("countryCell") as! ExploreCell!
        if countryCell == nil {
            countryCell = ExploreCell(style: UITableViewCellStyle.Default, reuseIdentifier: "countryCell")
        }
       

// Display Name
        
        if let exploreName = object?["State"] as? String {
            countryCell.countryName.text = exploreName
            
                // Display Image
                if let exploreImage = object?["Image"] as? PFFile {
                    exploreImage.getDataInBackgroundWithBlock {
                        (imageData: NSData?, error: NSError?) -> Void in
                        if error == nil {
                            countryCell.countryImage.image = UIImage(data:imageData!)
                            
                            
                        }
                    }
                }}
     return countryCell
        }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
            
        //parallax
        let offsetY = self.tableView.contentOffset.y
        for cell in self.tableView.visibleCells as! [ExploreCell] {
            let x = cell.countryImage.frame.origin.x
            let w = cell.countryImage.bounds.width
            let h = cell.countryImage.bounds.height
            let y = ((offsetY - cell.frame.origin.y) / h) * 10
            cell.countryImage.frame = CGRectMake(x, y, w, h)
        }
    }

        
    
    }


extension String {
    
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substringWithRange(Range(start: startIndex.advancedBy(r.startIndex), end: startIndex.advancedBy(r.endIndex)))
    }
}


