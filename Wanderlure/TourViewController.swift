//
//  TourViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 12/5/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit


class TourViewController: UIViewController {
    
    var listingsArray = [PFObject]()
    
    @IBOutlet weak var listingTableView: UITableView!
    
    @IBAction func switchTraveler(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
           }
    
    @IBAction func addButton1(sender: AnyObject) {
        performSegueWithIdentifier("addListing", sender: self)
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Remove Empty Cells
        self.listingTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        //Query
        let tourQuery = PFQuery(className:"Tour")
        tourQuery.whereKey("guide", equalTo: PFUser.currentUser()!)
        tourQuery.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                self.listingsArray = objects as! [PFObject]
                    }
            self.listingTableView.reloadData()
            }
        
        }


    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return listingsArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       
        let cell = tableView.dequeueReusableCellWithIdentifier("tourCell") as! CustomTableViewCell
        
        let listingObject = listingsArray[indexPath.row]
        
        let photo = listingObject["image"] as? PFFile
           photo!.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    cell.customImage.image = UIImage(data:imageData!)
                    cell.customText.text = listingObject["title"] as? String

            }}

        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        performSegueWithIdentifier("tourEdit", sender: self)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "tourEdit"){
            let indexPath: NSIndexPath = listingTableView.indexPathForSelectedRow!
            let object: PFObject = self.listingsArray[indexPath.row] 
            let theDestination = segue.destinationViewController as! ListTour2
            theDestination.Tour = object
        }
        
    }


    
    

}
