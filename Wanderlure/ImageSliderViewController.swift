//
//  ImageSliderViewController.swift
//  Wanderlure
//
//  Created by Sonny Chen on 12/4/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit
    
    class ImageSliderViewController: UIViewController {
        @IBOutlet var scrollView: UIScrollView!
        @IBOutlet var pageControl: UIPageControl!
        
        var Attraction: PFObject!
        var pageImages: [UIImage] = []
        var pageViews: [UIImageView?] = []
        var photosArray: [PFObject] = []
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            
            let imageQuery = PFQuery(className: "Photos")
            imageQuery.whereKey("attraction", equalTo: Attraction)
            imageQuery.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                self.photosArray = objects as! [PFObject]
                for object in self.photosArray {
                    let photo = object["photo"] as! PFFile
                    photo.getDataInBackgroundWithBlock {
                        (imageData: NSData?, error: NSError?) -> Void in
                        if error == nil {
                            self.pageImages.append(UIImage(data:imageData!)!)
                            self.navigationItem.title = "1 of " + String(self.pageImages.count)
                            
                            // 1
                            
                            
                            let pageCount = self.pageImages.count
                            
                            // 2
                            self.pageControl.currentPage = 0
                            self.pageControl.numberOfPages = pageCount
                            
                            // 3
                            for _ in 0..<pageCount {
                                self.pageViews.append(nil)
                            }
                            
                            // 4
                            let pagesScrollViewSize = self.scrollView.frame.size
                            self.scrollView.contentSize = CGSize(width: pagesScrollViewSize.width * CGFloat(self.pageImages.count),
                                height: pagesScrollViewSize.height)
                            
                            // 5
                            self.loadVisiblePages()
                        }}}}
            
        }
        
        
        
        
        func loadPage(page: Int) {
            if page < 0 || page >= pageImages.count {
                // If it's outside the range of what you have to display, then do nothing
                return
            }
            
            // 1
            if let pageView = pageViews[page] {
                // Do nothing. The view is already loaded.
            } else {
                // 2
                var frame = scrollView.bounds
                frame.origin.x = frame.size.width * CGFloat(page)
                frame.origin.y = 0.0
                
                // 3
                let newPageView = UIImageView(image: pageImages[page])
                newPageView.contentMode = .ScaleAspectFit
                newPageView.frame = frame
                scrollView.addSubview(newPageView)
                
                // 4
                pageViews[page] = newPageView
            }
        }
        
        func purgePage(page: Int) {
            if page < 0 || page >= pageImages.count {
                // If it's outside the range of what you have to display, then do nothing
                return
            }
            
            // Remove a page from the scroll view and reset the container array
            if let pageView = pageViews[page] {
                pageView.removeFromSuperview()
                pageViews[page] = nil
            }
        }
        
        func loadVisiblePages() {
            // First, determine which page is currently visible
            let pageWidth = scrollView.frame.size.width
            let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
            
            // Update the page control
            pageControl.currentPage = page
            
            // Work out which pages you want to load
            let firstPage = page - 1
            let lastPage = page + 1
            
            // Purge anything before the first page
            for var index = 0; index < firstPage; ++index {
                purgePage(index)
            }
            
            // Load pages in our range
            for index in firstPage...lastPage {
                loadPage(index)
            }
            
            // Purge anything after the last page
            for var index = lastPage+1; index < pageImages.count; ++index {
                purgePage(index)
            }
        }
        
        func scrollViewDidScroll(scrollView: UIScrollView!) {
            // Load the pages that are now on screen
            loadVisiblePages()
            self.navigationItem.title = String(pageControl.currentPage + 1) + " of " + String(pageImages.count)
            
        }
        
}
