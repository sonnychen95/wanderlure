//
//  ExploreCell.swift
//  Wanderlure
//
//  Created by Sonny Chen on 8/28/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit

class ExploreCell: UITableViewCell {
    
    @IBOutlet weak var exploreName: UILabel!
    @IBOutlet weak var exploreImage: UIImageView!
    @IBOutlet weak var numberOfListings: UILabel!
    @IBOutlet weak var stateName: UILabel!
    
    @IBOutlet weak var countryContainer: UIView!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var container: UIView!
    
    override func awakeFromNib() {
        
        
        self.countryContainer?.clipsToBounds = true
        self.container?.clipsToBounds = true
        }
   }

