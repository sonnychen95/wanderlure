//
//  BookingCell.swift
//  Wanderlure
//
//  Created by Sonny Chen on 11/14/15.
//  Copyright © 2015 Sonny Chen. All rights reserved.
//

import UIKit

class BookingCell: UITableViewCell {

    @IBOutlet weak var bookingImage: UIImageView!
    @IBOutlet weak var bookingDescription: UILabel!
    @IBOutlet weak var bookingReviews: UILabel!
    @IBOutlet weak var hostContainer: UIView!
    @IBOutlet weak var hostPhoto: UIImageView!
    @IBOutlet weak var bookingCost: UILabel!
    
    override func awakeFromNib() {
        self.hostPhoto.layer.borderWidth = 1
        self.hostPhoto.layer.borderColor = UIColor.whiteColor().CGColor
        self.hostPhoto?.clipsToBounds = true
        self.hostPhoto.layer.cornerRadius = 30
    }
}
