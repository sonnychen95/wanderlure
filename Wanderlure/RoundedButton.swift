//
//  RoundedButton.swift
//  Wanderlure
//
//  Created by Sonny Chen on 8/30/15.
//  Copyright (c) 2015 Sonny Chen. All rights reserved.
//

import UIKit
import Foundation

class RoundedButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let borderBlue = UIColor(red: 27.0/255.0, green: 162.0/255.0, blue: 182.0/255.0, alpha: 1.0)
        self.layer.borderColor = borderBlue.CGColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 4.0;
        
        
    }
}